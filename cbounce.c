/*
* (C) 2003-15 - ntop 
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

#define _GNU_SOURCE
#include <signal.h>
#include <sched.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <errno.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <netinet/ip_icmp.h>

#include <pcap.h>
#include "ndpi_api.h"


#define ALARM_SLEEP             1
#define MAX_CARD_SLOTS      32768
#define GTP_U_V1_PORT        2152
static struct timeval startTime;
u_int8_t bidirectional = 0, wait_for_packet = 1, flush_packet = 0, do_shutdown = 0, verbose = 0;
static pcap_t *fp, *fp1;






struct gtphdr {
	u_int8_t flags;
	u_int8_t message_type;
	u_int16_t message_len;
};

/* ******************************** */

void print_stats() {
	struct timeval endTime;
	double deltaMillisec;
	static u_int8_t print_all;
	static u_int64_t lastPkts = 0;
	static u_int64_t lastBytes = 0;
	double diff, bytesDiff;
	static struct timeval lastTime;
	char buf1[64], buf2[64], buf3[64];
	unsigned long long nBytes = 0, nPkts = 0;
	int i;

	if(startTime.tv_sec == 0) {
		gettimeofday(&startTime, NULL);
		print_all = 0;
	} else
	print_all = 1;

	gettimeofday(&endTime, NULL);

	
	fprintf(stderr, "=========================\n\n");

	lastPkts = nPkts, lastBytes = nBytes;

	lastTime.tv_sec = endTime.tv_sec, lastTime.tv_usec = endTime.tv_usec;
}

/* *************************************** */

void printHelp(void) {
	printf("cbounce - (C) 2016 Demokritos\n");
	printf("Using LibPCAP v.%s\n");
	printf("A packet forwarder application between interfaces.\n\n");
	printf("Usage:  cbounce -i <device> -o <device> -c <cluster id> [-b]\n"
	"                [-h] [-g <core id>] [-f] [-v] [-a]\n\n");
	printf("-h              Print this help\n");
	printf("-i <device>     Ingress device name\n");
	printf("-o <device>     Egress device name\n");
	printf("-c <cluster id> cluster id\n");
	printf("-b              Bridge mode (forward in both directions)\n");
	printf("-g <core id>    Bind this app to a core (with -b use <core id>:<core id>)\n");
	printf("-a              Active packet wait\n");
	printf("-f              Flush packets immediately\n");
	printf("-v              Verbose\n");
	exit(-1);
}

/* *************************************** */

static int in_cksum(u_short *addr, int len)
{
	register int nleft = len;
	register u_short *w = addr;
	register int sum = 0;
	u_short answer = 0;

	/*
	* Our algorithm is simple, using a 32 bit accumulator (sum), we add
	* sequential 16 bit words to it, and at the end, fold back all the
	* carry bits from the top 16 bits into the lower 16 bits.
	*/
	while (nleft > 1)  {
		sum += *w++;
		nleft -= 2;
	}

	/* mop up an odd byte, if necessary */
	if (nleft == 1) {
		*(u_char *)(&answer) = *(u_char *)w ;
		sum += answer;
	}

	/* add back carry outs from top 16 bits to low 16 bits */
	sum = (sum >> 16) + (sum & 0xffff); /* add hi 16 to low 16 */
	sum += (sum >> 16);         /* add carry */
	answer = ~sum;              /* truncate to 16 bits */
	return(answer);
}

static u_char *gtph = NULL, *gtph_bak = NULL;
u_int ipoffset_stat = 0;
int test = 0;
int gtphs;
u_int32_t gtpsaddr, gtpdaddr, innersaddr, innerdaddr; 

typedef struct TCP_Pseudo {
	struct in_addr src_ip; /* source ip */
	struct in_addr dest_ip; /* destination ip */
	uint8_t zeroes; /* = 0 */
	uint8_t protocol; /* = 6 */
	uint16_t len; /* length of TCPHeader */
} __attribute__ ((packed));

#define BUFSIZE 4096

struct pseudo_tcphdr
{
	unsigned int ip_src;
	unsigned int ip_dst;
	unsigned char zero;
	unsigned char protocol;
	unsigned short tcp_len;
};

unsigned short tcp_sum_calc(unsigned short len_tcp, unsigned short *src_addr, unsigned short *dest_addr, unsigned short *buff)
{
	
	unsigned short prot_tcp = 6;
	long sum;
	int i;
	sum = 0;
	
	/* Check if the tcp length is even or odd.  Add padding if odd. */
	if((len_tcp % 2) == 1){
		buff[len_tcp] = 0;  // Empty space in the ip buffer should be 0 anyway.
		len_tcp += 1; // incrase length to make even.
	}
	
	/* add the pseudo header */	
	sum += ntohs(src_addr[0]);
	sum += ntohs(src_addr[1]);
	sum += ntohs(dest_addr[0]);
	sum += ntohs(dest_addr[1]);
	sum += len_tcp; // already in host format.
	sum += prot_tcp; // already in host format.
	
	/* 
	* calculate the checksum for the tcp header and payload
	* len_tcp represents number of 8-bit bytes, 
	* we are working with 16-bit words so divide len_tcp by 2. 
	*/
	for(i=0;i<(len_tcp/2);i++){
		sum += ntohs(buff[i]);
	}
	
	// keep only the last 16 bits of the 32 bit calculated sum and add the carries
	sum = (sum & 0xFFFF) + (sum >> 16);
	sum += (sum >> 16);
	
	// Take the bitwise complement of sum
	sum = ~sum;

	return htons(((unsigned short) sum));
}

unsigned short in_checksum(unsigned short *buffer, int size)
{
	unsigned long cksum = 0;

	while(size >1) {
		cksum += *buffer++;
		size  -= sizeof(unsigned short);
	}
	if(size)
	cksum += *(unsigned char *)buffer;

	cksum  = (cksum >> 16) + (cksum & 0xffff);
	cksum += (cksum >>16);
	return (unsigned short)(~cksum);
}

unsigned short tcp_checksum(struct iphdr* iph, struct tcphdr* tcph, 
char* data, int size)
{
	char buffer[65536];
	struct pseudo_tcphdr psd_header;

	psd_header.ip_dst   = iph->daddr;
	psd_header.ip_src   = iph->saddr;
	psd_header.zero     = 0;
	psd_header.protocol = iph->protocol;
	psd_header.tcp_len  = htons(sizeof(struct tcphdr)+size);

	tcph->check = 0;
	memcpy(buffer, &psd_header, sizeof(struct pseudo_tcphdr));
	memcpy(buffer+sizeof(struct pseudo_tcphdr), tcph, 
	sizeof(struct tcphdr));
	memcpy(buffer+sizeof(struct pseudo_tcphdr)+sizeof(struct tcphdr), 
	data, size);
	return tcph->check = in_checksum((unsigned short *)buffer,
	sizeof(struct pseudo_tcphdr)+sizeof(struct tcphdr)+size);
}


static u_char teid_s[4]; //global TEID of the sender, spoof athonet
static u_int16_t ipsize_req, udpsize_req; //needed for the recalculation of the sizes for the repackaging

void DecToHexStr(int dec, char *str)
{ sprintf(str, "%X", dec); }


void packet_consumer_thread(u_char *interface,const struct pcap_pkthdr* pkthdr,const u_char*
packet) {
	
	int tx_queue_not_empty = 0;
	int pass = 1;
	
	printf("userdata %s\n", interface);
	
	u_int16_t ip_offset = 14, ip_len, eth_offset=0;
	u_int8_t proto = 0;
	
	struct ndpi_ethhdr *ethernet;
	u_char *pkt_data = packet;
	
	ethernet = (struct ndpi_ethhdr *) &pkt_data[eth_offset];
	u_char ether[sizeof(struct ndpi_ethhdr)];
	
	struct ndpi_iphdr *iph;
	
	iph = (struct ip_header*)(pkt_data + sizeof(struct ndpi_ethhdr));
	proto = iph->protocol;
	
	ip_len = ((u_short)iph->ihl * 4);
	
	if(0){ //TODO Packet GTP Repackaging
		
		
		if(iph->saddr == 1191515564){
			
			
			if(ipoffset_stat > 14){//if(strlen((u_char*)gtph)>0 || strlen((u_char*)gtph_bak)>0){
				
				u_char *packetaf = malloc(sizeof(u_char)*gtphs*(ntohs(iph->tot_len)));
				
				struct ndpi_ethhdr *ethn;
				ethn = (struct ndpi_ethhdr*) &gtph[0];
				u_char tempdest[6];
				//Reverse GTP MAC
				tempdest[0] = ethn->h_dest[0];
				tempdest[1] = ethn->h_dest[1];
				tempdest[2] = ethn->h_dest[2];
				tempdest[3] = ethn->h_dest[3];
				tempdest[4] = ethn->h_dest[4];
				tempdest[5] = ethn->h_dest[5];
				
				ethn->h_dest[0] = ethn->h_source[0];
				ethn->h_dest[1] = ethn->h_source[1];
				ethn->h_dest[2] = ethn->h_source[2];
				ethn->h_dest[3] = ethn->h_source[3];
				ethn->h_dest[4] = ethn->h_source[4];
				ethn->h_dest[5] = ethn->h_source[5];
				
				ethn->h_source[0] = tempdest[0];
				ethn->h_source[1] = tempdest[1];
				ethn->h_source[2] = tempdest[2];
				ethn->h_source[3] = tempdest[3];
				ethn->h_source[4] = tempdest[4];
				ethn->h_source[5] = tempdest[5];
				
				//Replace/Reverse original Inner IP
				//iph->saddr = innerdaddr;
				//printf("current saddr %u daddr %u \n", iph->saddr, iph->daddr); 

				//Reverse GTP IP
				struct ndpi_iphdr *iphn;
				iphn = (struct ip_header*) (gtph + sizeof(struct ndpi_ethhdr));
				iphn->saddr = gtpdaddr; 
				iphn->daddr = gtpsaddr;
				iphn->ttl -= 1;
				iphn->id = 0;
				iphn->tos = 0;
				iphn->check = 0;
				iphn->check = in_cksum((struct ndpi_iphdr *) iphn, 4*iphn->ihl);
				//GTP TEID modification
				
				/*int i;
						for(i = 0; i < 4; i++)
						printf("%02X ", teid_s[i]);
						printf("TEID of teid_s\n");*/
				
				//We take one TEID from the ATHONET to IPACCESS and store it, then use that to communicate smoothly
				gtph[46] = teid_s[0];//0x20;
				gtph[47] = teid_s[1];//0x00;
				gtph[48] = teid_s[2];//0x00;
				gtph[49] = teid_s[3];//0x0a;
				
				
				memcpy(packetaf, &gtph[0], gtphs);
				memcpy(packetaf+gtphs, &pkt_data[14], (ntohs(iph->tot_len)));
				//memcpy(packetaf, &pkt_data[0], (ntohs(iph->tot_len)));
				
				u_char teid[4];
				
				memcpy(teid, &packetaf[46], 4);
				int j;
				/*for(j = 0; j < 4; j++)
						printf("%02X ", teid[j]);
						printf("TEID of new packet\n");*/

				struct ndpi_iphdr *iphin;
				iphin = (struct ip_header*) &packetaf[gtphs];
				
				struct ndpi_udphdr *udp = (struct ndpi_udphdr *)(packetaf + sizeof(struct ndpi_ethhdr) + sizeof(struct ndpi_iphdr));
				
				
				// Need to recalculate the different IP size of the request vs the response,
				// then modify accordingly the UDP header <of the GTP> size
				
				struct ndpi_iphdr *iphn2;
				iphn2 = (struct ip_header *) &packetaf[14];
				
				u_int16_t ipsize_diff;
				u_int16_t ipsizeor;
				u_int16_t num = 36;
				printf("\n\nbefore iphin tot_len %u \n\n", ntohs(iphin->tot_len));
				iphn2->tot_len = iphin->tot_len;//udp->len;
				printf("\n\nafter iphn2 tot_len %u \n", ntohs(iphn2->tot_len));
				printf("-----0--------IPHN2 %u IPHNIN %u\n", ntohs(iphn2->tot_len), ntohs(iphin->tot_len));
				ipsizeor = ntohs(iphn2->tot_len) + num;
				iphn2->tot_len = htons(ipsizeor);
				if(iphn2->tot_len != iphin->tot_len){
					printf("-----1--------IPHN2 %u IPHNIN %u\n", ntohs(iphn2->tot_len), ntohs(iphin->tot_len));
					
				}
				udp->len = iphn2->tot_len;
				if(iphn2->tot_len < iphin->tot_len){
					printf("---2----------IPHN2 %u IPHNIN %u\n", ntohs(iphn2->tot_len), ntohs(iphin->tot_len));
				}
				udp->len -= htons(20);
				if(iphn2->tot_len < iphin->tot_len) printf("--3-----------IPHN2 %u IPHNIN %u\n", ntohs(iphn2->tot_len), ntohs(iphin->tot_len));
				iphn2->check = 0;
				iphn2->check = in_cksum((struct ndpi_iphdr *) iphn2, 4*iphn2->ihl);
				if(iphn2->tot_len < iphin->tot_len) printf("----4---------IPHN2 %u IPHNIN %u\n", ntohs(iphn2->tot_len), ntohs(iphin->tot_len));
				//printf("\n\nmodify ip-udp length %u %u \n\n", ntohs(iphn2->tot_len), ntohs(udp->len));
				//}
				if(iphn2->tot_len < iphin->tot_len) printf("------5-------IPHN2 %u IPHNIN %u\n", ntohs(iphn2->tot_len), ntohs(iphin->tot_len));
				//GTP length
				struct gtphdr *gtpnh = (struct gtphdr *) &packetaf[42];
				gtpnh->message_len = iphin->tot_len;
				if(iphn2->tot_len < iphin->tot_len) printf("----6---------IPHN2 %u IPHNIN %u\n", ntohs(iphn2->tot_len), ntohs(iphin->tot_len));
				//udp->len = ntohs(iphin->tot_len);//UDP data length htons(8 + strlen(data));					
				iphin->daddr = innersaddr;
				iphin->ttl -= 1;
				iphin->check = 0;
				iphin->check = in_cksum((struct ndpi_iphdr *) iphin, 4*iphin->ihl);
				
				if(proto == IPPROTO_TCP){
					struct ndpi_tcphdr *tcp;
					tcp = (struct ndpi_tcphdr*) &packetaf[gtphs+20];
					tcp->check = 0;
					tcp->check = (unsigned short) tcp_checksum( iphin, tcp, (u_char *)tcp+20, ntohs(iphin->tot_len) - sizeof(struct ndpi_iphdr) - sizeof(struct tcphdr));
					//printf("tcp checksum after  %02x\n", tcp->check);
					//printf("calculated size %u\n", ntohs(iphn->tot_len) - sizeof(struct ndpi_iphdr) - sizeof(struct tcphdr));
					
					pass = 1;
					
					/*for(j = 0; j < sizeof ethernet + ntohs(iph->tot_len); j++)
							printf("%02X ", packetaf[j]);
							printf("\n");*/
					
					
					if (pcap_sendpacket(fp1, packetaf, gtphs+(ntohs(iph->tot_len))) != 0) //+ntohs(iph->tot_len)+14
					{
						//fprintf(stderr,"\nError sending the packet: \n", pcap_geterr(_pcap_file[0]));
						//return;
					}
				}
				else if(proto == IPPROTO_ICMP){
					//The buf is for the icmp checksum calculation
					u_char *buf = (u_char *) (packetaf + sizeof(struct ndpi_ethhdr));
					
					struct icmphdr *icmp;
					icmp = (struct icmphdr*) &packetaf[gtphs+20];
					printf("icmp type %u code %u \n", icmp->type, icmp->code);
					icmp->checksum = 0;
					
					
					for(j = 0; j < gtphs+(ntohs(iph->tot_len)); j++)
							printf("%02X ", buf[j]);
						printf("\nicmp checksum\n");

					icmp->checksum = in_cksum(buf, 64);
					
					pass = 1;
					
					
					/*for(j = 0; j < sizeof ethernet + ntohs(iph->tot_len); j++)
							printf("%02X ", packetaf[j]);
							printf("\n");*/
					
					
					if (pcap_sendpacket(fp1, packetaf, gtphs+(ntohs(iph->tot_len))) != 0) //+ntohs(iph->tot_len)+14
					{
						//fprintf(stderr,"\nError sending the packet: \n", pcap_geterr(_pcap_file[0]));
						//return;
					}
				}
				
				
				free(packetaf);
			}
		}
	}
	
	if(0){ //TODO
		
		struct ndpi_udphdr *udp = (struct ndpi_udphdr *)(pkt_data + sizeof(struct ndpi_ethhdr) + sizeof(struct ndpi_iphdr));
		u_int16_t sport = ntohs(udp->source), dport = ntohs(udp->dest);

		if((sport == GTP_U_V1_PORT) || (dport == GTP_U_V1_PORT)) {
			/* Check if it's GTPv1 */
			u_int offset = ip_offset+ip_len+sizeof(struct ndpi_udphdr);
			u_int8_t flags = pkt_data[offset];
			u_int8_t message_type = pkt_data[offset+1];

			if((((flags & 0xE0) >> 5) == 1 /* GTPv1 */) &&
					(message_type == 0xFF /* T-PDU */)) {

				ip_offset = ip_offset+ip_len+sizeof(struct ndpi_udphdr)+8; /* GTPv1 header len */
				if(flags & 0x04) ip_offset += 1; /* next_ext_header is present */
				if(flags & 0x02) ip_offset += 4; /* sequence_number is present (it also includes next_ext_header and pdu_number) */
				if(flags & 0x01) ip_offset += 1; /* pdu_number is present */
			}
			if(pkt_data[offset+4]>0 && pkt_data[offset+7]){
				//printf("Packet from Athonet->IPACCESS, so TEID of sender \n");
				memcpy(teid_s, &pkt_data[offset+4], 4);
				int j;
				/*for(j = 0; j < 4; j++)
						printf("%02X ", teid_s[j]);
						printf("\n");*/
			}
		}
	}
	
	
	
	if((proto == IPPROTO_UDP)) {
		
		struct ndpi_udphdr *udp = (struct ndpi_udphdr *)(pkt_data + sizeof(struct ndpi_ethhdr) + sizeof(struct ndpi_iphdr));
		u_int16_t sport = ntohs(udp->source), dport = ntohs(udp->dest);
		
		//printf("sport %u dport %u\n", sport, dport);
		
		if((sport == GTP_U_V1_PORT) || (dport == GTP_U_V1_PORT)) {
			
			
			/* Check if it's GTPv1 */
			u_int offset = ip_offset+ip_len+sizeof(struct ndpi_udphdr);
			u_int8_t flags = pkt_data[offset];
			u_int8_t message_type = pkt_data[offset+1];
			
			if((((flags & 0xE0) >> 5) == 1 /* GTPv1 */) &&
					(message_type == 0xFF /* T-PDU */)) {

				ip_offset = ip_offset+ip_len+sizeof(struct ndpi_udphdr)+8; /* GTPv1 header len */
				if(flags & 0x04) ip_offset += 1; /* next_ext_header is present */
				if(flags & 0x02) ip_offset += 4; /* sequence_number is present (it also includes next_ext_header and pdu_number) */
				if(flags & 0x01) ip_offset += 1; /* pdu_number is present */
			}
			
			u_char teid[4];
			memcpy(teid, &pkt_data[offset+4], 4);
			
			if(ip_offset > 0){
				int j;
				gtph = malloc(ip_offset);
				memcpy(gtph, &pkt_data[0], ip_offset);
				gtph[ip_offset] = '\0';
				gtphs = ip_offset;
				
				ipoffset_stat = ip_offset;
				
				/*for(j = 0; j < ip_offset; j++)
						printf("%02X ", gtph[j]);
						printf("GTPH \n");*/
			}
			
			
			//reserve GTP IP flow for re-capsulation
			gtpsaddr = iph->saddr;
			gtpdaddr = iph->daddr;
			
			iph = (struct ndpi_iphdr *) &pkt_data[ip_offset];
			
			//printf("2.CHECK  ---------------Packet size %u gtph %u ipoffset %u\n ",ntohs(iph->tot_len), strlen((u_char*)gtph), ip_offset);
			
			//Store the request sizes of the ip and udp, to recalculate for the responses
			udpsize_req = udp->len;
			ipsize_req = iph->tot_len;
			
			//printf("ip dst %u\n", iph->daddr);
			//1709435279 227.101
			//1759766927 227.104
			//1826875791 227.108
			
			//1191515564 172.21.5.71
			if(iph->daddr == 1191515564){
				
				u_char ether[14];
				memcpy(ether, &pkt_data[0], 14);
				ether[14] = '\0';
				
				//printf("packet size %u offset %u\n",ntohs(iph->tot_len), ip_offset);
				u_char iphc[ntohs(iph->tot_len)];
				memcpy(iphc, &pkt_data[ip_offset], ntohs(iph->tot_len));
				iphc[ntohs(iph->tot_len)] = '\0';
				
				
				u_char *packetaf = malloc(sizeof(u_char)*14*ntohs(iph->tot_len));
				//asprintf(&packetaf, "%s%s", ether, iphn);
				
				memcpy(packetaf, ether, 14);
				packetaf[14] = '\0';
				memcpy(packetaf+14, &pkt_data[ip_offset], ntohs(iph->tot_len));
				
				struct ndpi_ethhdr *ethn;
				ethn = (struct ndpi_ethhdr*) &packetaf[0];
				
				struct ndpi_iphdr *iphn;
				iphn = (struct ip_header*) (packetaf + sizeof(struct ndpi_ethhdr));
				//printf("ip dst of internal %u\n", iphn->daddr);
				
				// 227.101
				/*ethn->h_dest[0]= 0xb8;
						ethn->h_dest[1]= 0xca;
						ethn->h_dest[2]= 0x3a;
						ethn->h_dest[3]= 0x83;
						ethn->h_dest[4]= 0x91;
						ethn->h_dest[5]= 0xf0;
						
						// 227.104
						// ethn->h_dest[0]= 0x08;
						// ethn->h_dest[1]= 0x00;
						// ethn->h_dest[2]= 0x27;
						// ethn->h_dest[3]= 0x70;
						// ethn->h_dest[4]= 0x4c;
						// ethn->h_dest[5]= 0x76;
						//08:00:27:70:4c:76
						
						// 227.108*/
				/*ethn->h_dest[0]= 0x08;
						ethn->h_dest[1]= 0x00;
						ethn->h_dest[2]= 0x27;
						ethn->h_dest[3]= 0x96;
						ethn->h_dest[4]= 0xdd;
						ethn->h_dest[5]= 0xd0;*/
				
				//08:00:27:96:dd:d0
				
				ethn->h_dest[0]= 0x00;
				ethn->h_dest[1]= 0x11;
				ethn->h_dest[2]= 0x6b;
				ethn->h_dest[3]= 0x95;
				ethn->h_dest[4]= 0xa6;
				ethn->h_dest[5]= 0x61;
				//00:11:6b:95:a6:61 shuttle
				
				/*ethn->h_source[0]= 0x00;
						ethn->h_source[1]= 0x24;
						ethn->h_source[2]= 0xe8;
						ethn->h_source[3]= 0x19;
						ethn->h_source[4]= 0x9c;
						ethn->h_source[5]= 0x3c;*/
				
				
				//Inner IPs
				
				innersaddr = iphn->saddr;
				innerdaddr = iphn->daddr;
				
				//iphn->saddr = 1977870735; //143.233.227.117
				//00:24:e8:19:9c:3c
				
				iphn->check = 0;
				iphn->check = in_cksum((struct ndpi_iphdr *) iphn, 4*iphn->ihl);
				
				
				u_char *buf = (u_char *) (packetaf + sizeof(struct ndpi_ethhdr) + 10
				+ sizeof(struct ndpi_iphdr) +  sizeof(struct ndpi_iphdr) + sizeof(struct ndpi_tcphdr));
				
				struct tcphdr* tcp;
				tcp = (struct tcphdr*) (packetaf + sizeof(struct ndpi_ethhdr) + sizeof(struct ndpi_iphdr));
				
				tcp->check = 0;
				//tcp->check = tcp_sum_calc((iphn->tot_len - iphn->ihl*4 - tcp->doff*4), iphn->saddr, iphn->daddr, &packetaf);//in_cksum(tcp, totaltcp_len);
				
				tcp->check = (unsigned short) tcp_checksum( iphn, tcp, (u_char *)tcp+20, ntohs(iphn->tot_len) - sizeof(struct ndpi_iphdr) - sizeof(struct tcphdr));
				
				u_char *data = (u_char *)tcp+20;
				
				int j;
				/*for(j = 0; j <  ntohs(iphn->tot_len); j++)
						printf("%02X ", pkt_data[j]);
						
						printf("\nRemix\n");
						for(j = 0; j <  74; j++)
						printf("%02X ", packetaf[j]);
						printf("\n");
						
						printf("\nData\n");
						for(j = 0; j <  ntohs(iphn->tot_len) - sizeof(struct ndpi_iphdr) - sizeof(struct ndpi_tcphdr); j++)
						printf("%02X ", data[j]);
						printf("\n");
						
						printf("GTP decap \n");*/
				pass = 0;
				if (pcap_sendpacket(fp, packetaf, 14+ntohs(iphn->tot_len)) != 0)
				{
					//fprintf(stderr,"\nError sending the packet: \n", pcap_geterr(_pcap_file[0]));
					//return;
				}
				
				free(packetaf);
				
			}
		}	
	}

	errno = 0;
	
	//if(pass)
	
	if (wait_for_packet) usleep(1);
	
	pass = 1;
	
	tx_queue_not_empty = 1;
	
	if(strcmp(interface, "eth0")){
		
		if (pcap_sendpacket(fp, packet, 1500) != 0) //+ntohs(iph->tot_len)+14
		{
			fprintf(stderr,"\nError sending the packet: \n", pcap_geterr(fp));
			return;
		}
	}
	else if(strcmp(interface, "eth1")){
		if (pcap_sendpacket(fp1, packet, 1500) != 0) //+ntohs(iph->tot_len)+14
		{
			fprintf(stderr,"\nError sending the packet: \n", pcap_geterr(fp1));
			return;
		}
	}
	
	//free(packet);
	
	return NULL;
}

char *device1 = NULL, *device2 = NULL, *device3 = "eth3";

void* pcapLooper(void* param)
{
	pcap_t* handler = (pcap_t*) param;
	//pcap_loop(handler, 900 ,procPacket, NULL );
	pcap_loop(handler,-1,packet_consumer_thread,device1);
}

void* pcapLooper2(void* param)
{
	pcap_t* handler = (pcap_t*) param;
	//pcap_loop(handler, 900 ,procPacket, NULL );
	pcap_loop(handler,-1,packet_consumer_thread,device2);
}

/* *************************************** */

int main(int argc, char* argv[]) {
	
	char *bind_mask = NULL, c;
	int cluster_id = -1;
	u_int numCPU = sysconf( _SC_NPROCESSORS_ONLN );
	

	startTime.tv_sec = 0;

	while((c = getopt(argc,argv,"abc:g:hi:o:fv")) != '?') {
		if((c == 255) || (c == -1)) break;

		switch(c) {
		case 'h':
			printHelp();
			break;
		case 'a':
			wait_for_packet = 0;
			break;
		case 'f':
			flush_packet = 1;
			break;
		case 'v':
			verbose = 1;
			break;
		case 'b':
			bidirectional = 1;
			break;
		case 'c':
			cluster_id = atoi(optarg);
			break;
		case 'i':
			device1 = strdup(optarg);
			break;
		case 'o':
			device2 = strdup(optarg);
			break;
		case 'g':
			bind_mask = strdup(optarg);
			break;
		}
	}

	if (device1 == NULL) printHelp();
	if (device2 == NULL) printHelp();
	
	
	u_int snaplen = 1536;
	char errbuf[PCAP_ERRBUF_SIZE];
	pthread_t tid1, tid2;
	int ret1, ret2;
	
	fp = pcap_open_live(device1, snaplen, 0, 100, errbuf);
	fp1 = pcap_open_live(device2, snaplen, 0, 100, errbuf);
	
	ret1 = pthread_create(&tid1, NULL, pcapLooper, fp);
	ret2 = pthread_create(&tid2, NULL, pcapLooper2, fp1);
	
	//pthread_join(tid1, NULL);
	//pthread_join(tid2, NULL);

	
	if (!verbose) while (!do_shutdown) {
		sleep(ALARM_SLEEP);
		print_stats();
	}
	
	sleep(1);
	
	return 0;
}

