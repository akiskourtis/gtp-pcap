
#
# Search directories
#

INCLUDE    = -InDPI -InDPI/include -InDPI/src/include

#
# User and System libraries
#
LIBS       = -lpcap -lpthread  -lrt nDPI/src/lib/.libs/libndpi.a

#
# C compiler and flags
#
CC         = ${CROSS_COMPILE}gcc
CFLAGS     = -O2 -Wall -g -w  ${INCLUDE}



#
# Main targets
#
PFPROGS   = 

PFPROGS += cbounce 

TARGETS   =  ${PFPROGS}

all: ${TARGETS}

cbounce: cbounce.o Makefile
	${CC} ${CFLAGS} cbounce.o ${LIBS} -o $@

clean:
	@rm -f ${TARGETS} *.o *~ config.*
