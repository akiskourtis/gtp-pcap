# README #

### Build the PF_RING module ###
cd pfring-gtp/PF_RING/kernel

make

insmod ./pf_ring.ko

cd ../userland/examples_zc

make

### Load the Hugepages ###

echo 1024 > /sys/kernel/mm/hugepages/hugepages-2048kB/nr_hugepages

mkdir -p /mnt/huge

mount -t hugetlbfs nodev /mnt/huge

### Run the application with eth0 = eNB, eth1 = EPC, (needs to be present) eth3 = local-breakout ###

*In the folder PF_RING/userland/examples_zc

./zbounce -i eth0 -o eth1 -c 99 -g 1 -b